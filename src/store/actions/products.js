import axios from '../../axios-api';
import {CREATE_PRODUCT_SUCCESS, FETCH_PRODUCTS_SUCCESS, DELETE_PRODUCT_SUCCESS, SHOW_ONE_PRODUCT_SUCCESS} from "./actionTypes";

export const fetchProductsSuccess = products => {
  return {type: FETCH_PRODUCTS_SUCCESS, products};
};

export const fetchProducts = () => {
  return dispatch => {
    axios.get('/products').then(
      response => dispatch(fetchProductsSuccess(response.data))
    );
  }
};

export const createProductSuccess = () => {
  return {type: CREATE_PRODUCT_SUCCESS};
};

export const deleteProductSuccess = () => {
  return {type: DELETE_PRODUCT_SUCCESS};
};

export const createProduct = productData => {
  return dispatch => {
    return axios.post('/products', productData).then(
      response => dispatch(createProductSuccess())
    );
  };
};

export const deleteProduct = productData => {
  return dispatch => {
    return axios.delete('/products', productData).then(
      response => dispatch(deleteProductSuccess())
    );
  };
};

export const showOneProductSuccess = (product) => {
  return {type: SHOW_ONE_PRODUCT_SUCCESS, product};
};

export const showOneProduct = (id) => {
  return dispatch => {
    axios.get('/products/' + id + '.json').then(response => {
      const product = response.data;
      dispatch(showOneProductSuccess(product))
    })
  }
};