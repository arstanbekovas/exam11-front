import axios from '../../axios-api';
import {push} from 'react-router-redux';
import {NotificationManager} from 'react-notifications';
import {
  LOGIN_USER_FAILURE, LOGIN_USER_SUCCESS, LOGOUT_USER,
  REGISTER_USER_FAILURE, REGISTER_USER_SUCCESS
} from "./actionTypes";

const registerUserSuccess = () => {
  return {type: REGISTER_USER_SUCCESS};
};

const registerUserFailure = error => {
  return {type: REGISTER_USER_FAILURE, error};
};

export const registerUser = userData => {
  return dispatch => {
    return axios.post('/users', userData).then(
      response => {
        dispatch(registerUserSuccess());
        dispatch(push('/'));
        NotificationManager.success('Registration successful', 'Success', 3000);

      },
      error => {
        dispatch(registerUserFailure(error.response.data));
      }
    );
  };
};

const loginUserSuccess = user => {
  return {type: LOGIN_USER_SUCCESS, user};
};

const loginUserFailure = error => {
  return {type: LOGIN_USER_FAILURE, error};
};

export const loginUser = userData => {
  return dispatch => {
    return axios.post('/users/sessions', userData).then(
      response => {
        dispatch(loginUserSuccess(response.data.user));
        dispatch(push('/'));
        NotificationManager.success('Login successful', 'Success', 3000);

      },
      error => {
        const errorObj = error.response ? error.response.data : {error: 'No internet'};
        dispatch(loginUserFailure(errorObj));
      }
    )
  }
};

export const logoutUser = () => {
  return (dispatch, getState) => {
    const token = getState().users.user.token;
    const headers = {'Token': token};
    axios.delete('/users/sessions', {headers}).then(
      response => {
        dispatch({type: LOGOUT_USER});
        dispatch(push('/'));
        NotificationManager.success('Logout successful', 'Success', 3000);
      },
      error => {
        NotificationManager.error('Could not logout', 'Check internet connection', 3000);

      }
    );
  }
};