import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';
import {PageHeader} from "react-bootstrap";
import {deleteProduct, fetchProducts, showOneProduct, showOneProductSuccess} from "../../store/actions/products";
import axios from '../../axios-api';


import ProductListItem from '../../components/ProductListItem/ProductListItem';
import {NavLink} from "react-router-dom";
import ViewProduct from "../ViewProduct/ViewProduct";
import Modal from "../../components/UI/Modal/Modal";

class Products extends Component {
  state = {
    categories: [
      {title: 'Computers', id: '5affd3b5fc1c483428457a56'},
      {title: 'Cars', id: '5affd3b5fc1c483428457a57'},
      {title: 'Others', id: '5b000d5abeb5325917c6c89d'}
    ],
    category: null,
    products: [],
    showModal: false,
    selectedProduct: ''

  };

  _loadData = () => {
    console.log(this.props.match.params.category);
    let url = '/products';

    let category = this.props.match.params.category;

    if (category) {
      url = '/categories/' + category + '';
    } else {
      url = '/products';
    }

    axios.get(url).then((response) => {
      this.setState({
        products: response.data
      })
    })
  };

  showModal = (event, id) => {
    event.preventDefault();
    this.props.history.replace('/');
    this.setState({showModal: true, selectedProduct:id });
    this.props.onShowOneProduct(id)
  };

  purchaseCancelHandler = () => {
    this.setState({showModal: false});
  };
  productDeleteHandler = (id) => {
    this.props.onProductDeleted(id);
    this.props.history.replace('/');
    this.purchaseCancelHandler();
  };

  componentDidMount() {
    this._loadData();
    this.props.onFetchProducts();
  }

  componentDidUpdate() {
      if (this.props.match.params.category !== this.state.category) {
        this.setState({category: this.props.match.params.category});
        this._loadData();
      }
  }

  render() {
    console.log(this.state.selectedProduct.id);
    let modalContent;
    if(this.state.selectedProduct) {
      modalContent = (
        <ViewProduct
          id={this.props.products._id}
          name={this.props.products.title}
          phone={this.props.products.phone}
          description={this.props.products.description}
          image={this.props.products.image}
          delete={() => this.productDeleteHandler(this.props.products)}
        />
      )
    } else {
      modalContent = null;
    }
    return (

      <Fragment>
        <PageHeader> All items</PageHeader>
        <section><li><NavLink to={'/'}>All</NavLink></li></section>
        <section >
          {this.state.categories.map(category =>
            <li>
              <NavLink
                to={'/categories/' + category.id}
                key={category.id}>
                {category.title}
              </NavLink>
            </li>
          )}
        </section>

        {this.state.products.map(product => (
          <ProductListItem
            click={(event) => this.showModal(event, product._id)}
            key={product._id}
            id={product._id}
            title={product.title}
            price={product.price}
            image={product.image}
          />
        ))}

        <Modal
        show={this.state.showModal}
        closed={this.purchaseCancelHandler} >
        {modalContent}
      </Modal>
      </Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    products: state.products.products
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchProducts: () => dispatch(fetchProducts()),
    onProductDeleted: (itemId) => dispatch(deleteProduct(itemId)),
    onShowOneProduct: (itemId) => dispatch(showOneProduct(itemId)),


  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Products);