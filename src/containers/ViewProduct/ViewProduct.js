import React from 'react';
import NavLink from "react-router-dom/es/NavLink";

const ViewProduct = props => {
  return (
    <div>
      <img  src={props.image}/>
      <div >Item:{props.title}</div>
      <div >Phone: {props.phone}</div>
      <div >Description: {props.description}</div>
      <div >Category: {props.category}</div>
      <div >Price: {props.price}</div>
      <NavLink to={'/'} onClick={props.delete} >Delete</NavLink>
    </div>
  )
};

export default ViewProduct;