import React, {Component} from 'react';
import {
  Button, Col, ControlLabel, Form, FormControl, FormGroup
} from "react-bootstrap";



class ProductForm extends Component {
  state = {
    title: '',
    price: '',
    description: '',
    image: '',
    category: '',
    userId:''
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();
    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.onSubmit(formData);
    console.log(formData, "form data ")

  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    });
  };


  render() {
    return (

      <Form horizontal onSubmit={this.submitFormHandler}>
        <FormGroup controlId="productTitle">
          <Col componentClass={ControlLabel} sm={2}>
            Title
          </Col>
          <Col sm={10}>
            <FormControl
              type="text" required
              placeholder="Enter product title"
              name="title"
              value={this.state.title}
              onChange={this.inputChangeHandler}
            />
          </Col>
        </FormGroup>

        <FormGroup controlId="productPrice">
          <Col componentClass={ControlLabel} sm={2}>
            Price
          </Col>
          <Col sm={10}>
            <FormControl
              type="number" min="0" required
              placeholder="Enter product price"
              name="price"
              value={this.state.price}
              onChange={this.inputChangeHandler}
            />
          </Col>
        </FormGroup>

        <FormGroup controlId="productDescription">
          <Col componentClass={ControlLabel} sm={2}>
            Description
          </Col>
          <Col sm={10}>
            <FormControl
              componentClass="textarea"
              placeholder="Enter description"
              name="description"
              value={this.state.description}
              onChange={this.inputChangeHandler}
            />
          </Col>
        </FormGroup>

        <FormGroup controlId="productImage">
          <Col componentClass={ControlLabel} sm={2}>
            Image
          </Col>
          <Col sm={10}>
            <FormControl
              type="file"
              name="image"
              onChange={this.fileChangeHandler}
            />
          </Col>
        </FormGroup>

        <FormGroup controlId="productCategory">
          <Col componentClass={ControlLabel} sm={2}>
            Category
          </Col>
          <Col sm={10}>
            <select onChange={this.inputChangeHandler} name="category" value={this.state.category}>
              <option value="" selected disabled hidden>Choose category</option>
              <option value="5affd3b5fc1c483428457a56">Computers</option>
              <option value="5affd3b5fc1c483428457a57">Cars</option>
              <option value="5b000d5abeb5325917c6c89d">Others</option>
            </select>

          </Col>
        </FormGroup>

        <FormGroup>
          <Col smOffset={2} sm={10}>
            <Button bsStyle="primary" type="submit">Create item</Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}

export default ProductForm;
